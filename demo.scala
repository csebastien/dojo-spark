// Core Spark
val rdd = sc.textFile("file:///data/*.txt", 10)
rdd.flatMap(_.split("\\W+")).filter(_.nonEmpty).map(_.toLowerCase).map((_,1)).reduceByKey(_ + _).map(_.swap).sortByKey(false).take(10).foreach(println)




// Spark SQL
val ds = spark.read.text("file:///data/*.txt").as[String]
val ds2 = ds.flatMap(_.split("\\W+")).filter(_.nonEmpty).map(_.toLowerCase).groupBy("value").count().orderBy($"count".desc)
ds2.show

ds2.createOrReplaceTempView("wordCount")
spark.sql("SELECT * FROM wordCount ORDER BY count DESC").show
spark.sql("SELECT count AS wordCount, count(*) AS wordCountCount  FROM wordCount GROUP BY count ORDER BY wordCount ASC, wordCountCount ASC").show(50)




// Spark Streaming
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe

val ssc = new StreamingContext(sc, Seconds(2))

val kafkaParams = Map[String, Object](
  "bootstrap.servers" -> "kafka:9092",
  "key.deserializer" -> classOf[StringDeserializer],
  "value.deserializer" -> classOf[StringDeserializer],
  "group.id" -> "spark-test",
  "auto.offset.reset" -> "latest",
  "enable.auto.commit" -> (false: java.lang.Boolean)
)
val dstream = KafkaUtils.createDirectStream[String,String](ssc, PreferConsistent, Subscribe[String, String](Set("test"), kafkaParams)).map(_.value)
dstream.flatMap(_.split("\\W+")).filter(_.nonEmpty).map(_.toLowerCase).map((_,1)).reduceByKey(_ + _).map(_.swap).print

ssc.start
Thread.sleep(10000)
// docker exec -ti spark_kafka_1 bash
// cat /data/*.txt | /opt/kafka_*/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
ssc.stop(false)




// Spark Structured Streaming
val ss = spark.readStream.text("file:///project/feed/*.txt").as[String]
val query = ss.flatMap(_.split("\\W+")).filter(_.nonEmpty).map(_.toLowerCase).groupBy("value").count().orderBy($"count".desc).writeStream.outputMode("complete").format("memory").queryName("runningWordCount").start()
Thread.sleep(5000)
// cp data/pg1* feed/
// cp data/pg2* feed/
spark.sql("SELECT * FROM runningWordCount ORDER BY count DESC").show
query.stop

