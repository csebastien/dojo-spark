Spark Dojo
==========================

# Preparation
* Install docker (and docker-compose)
* Build/pull docker images
```
docker-compose build

docker-compose pull

docker-compose up -d

docker exec -ti spark_spark_1 spark-shell --packages org.apache.spark:spark-streaming-kafka-0-10_2.11:2.2.0
# Type ^D or :quit to exit
```

# Demo (Spoiler)
```
docker-compose up -d

./create-topic.sh

docker exec -ti spark_spark_1 spark-shell --packages org.apache.spark:spark-streaming-kafka-0-10_2.11:2.2.0
# :load /project/demo.scala

docker exec -ti spark_kafka_1 bash
# cat /data/*.txt | /opt/kafka_*/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
```
