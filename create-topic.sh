#!/bin/bash
docker exec -ti spark_kafka_1 /opt/kafka_2.11-0.8.2.1/bin/kafka-topics.sh --create --zookeeper localhost --topic "${1:-test}" --partitions 3 --replication-factor 1
